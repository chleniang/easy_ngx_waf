-- log_by_lua 阶段,处理 easy_ngx_waf 日志

local log_tbl = ngx.ctx.easy_ngx_waf_log
if (not log_tbl) or type(log_tbl) ~= "table" then
    return
end

local config = require("config")

-- 重新打开waf日志文件
if not g_log_file_handle then
    g_log_file_handle = io.open(config.waf_log_file, "a+")
    if not g_log_file_handle then
        ngx.log(ngx.ERR, "[easy_waf] log file open failed at ngx log phase.")
        return
    end
end

local json_safe = require("cjson.safe")
json_safe.encode_empty_table_as_object(false)
-- "/" 保持原样,不转义(false)
json_safe.encode_escape_forward_slash(false)

-- cjson encode()无法保证key的顺序,不用encode()
-- local ok, log_str = pcall(json_safe.encode, log_tbl)
-- if not ok then
--     return
-- end

-- cjson encode()无法保证key的顺序,日志文件不规范,此处改为手动拼接字符串方式
local log_str = '{"ip":"' .. (log_tbl["ip"] or "unknown")
    .. '","time":"' .. (log_tbl["time"] or "unknown")
    .. '","host":"' .. (log_tbl["host"] or "unknown")
    .. '","req":"' .. (log_tbl["req"] or "unknown")
    .. '","ua":"' .. (log_tbl["ua"] or "unknown") .. '"'

local end_str
local ok, msg_str = pcall(json_safe.encode, log_tbl["msg"])
if ok then
    end_str = ',"msg":' .. msg_str .. "}\n"
else
    end_str = ',"msg":[]' .. "}\n"
end

local res, err = g_log_file_handle:write(log_str .. end_str)
if res == nil then
    ngx.log(ngx.ERR, "[easy_ngx_war] write waf log ERR:" .. tostring(err) .. "\n >> log::\n" .. log_str .. end_str)
    g_log_file_handle:close()
else
    g_log_file_handle:flush()
end
