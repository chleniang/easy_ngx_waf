local _M = {}

local allow_act = {
    "deny",
    "next",
    "allow"
}

_M.is_valid = function(act)
    if type(act) ~= "string" then
        return false, "rule:act need string type(" .. type(act) .. " now)."
    end
    local _act = string.lower(act)
    for _, v in ipairs(allow_act) do
        if _act == v then
            return true
        end
    end
    return false, "rule:act invalid."
end

return _M
