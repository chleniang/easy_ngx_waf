---
-- 规则值

local _M = {}

---验证value合法性(不可为空/空白字符)
---<br>     如果value为数组,只要有一个不合法就会返回,后续元素不再验证
---<br>     返回 true/false, false时信息
---@param value string | table
---@return boolean
---@return string | nil
_M.is_valid = function(value)
    local t = type(value)
    if t == "nil" then
        return false, "rule:value is nil."
    elseif t == "table" then
        if #value < 1 then
            return false, "rule:value is empty table."
        end
        for _, v in ipairs(value) do
            local res, err = _M.is_valid(v)
            if not res then
                return res, err
            end
        end
    else
        if string.match(tostring(value), "^%s*$") then
            return false, "rule:value is empty string."
        end
    end

    return true
end

return _M
