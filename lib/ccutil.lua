---CC检测工具,调用接口
-- Author: chleniang@163.com

local _M = {}

local config = require("config")
local adapter = nil

_M.init = function()
    local t = string.lower(config.cc_dict_type)
    if t == "redis" then
        adapter = require("lib.cc_redis").new()
    elseif t == "shared_dict" then
        adapter = require("lib.cc_shared_dict").new()
    else
        ngx.log(ngx.ERR,
        "[easy_waf] cc_dict_type(" .. config.cc_dict_type .. ') ERROR. Just accepted "redis" or "shared_dict"')
        return
    end
end

_M.set = function(key, value, timeout)
    return adapter:set(key, value, timeout)
end

_M.incr = function(key)
    return adapter:incr(key)
end

_M.get = function(key)
    return adapter:get(key)
end

---拦截IP
---@param ip string 待拦截IP
---@param timeout number 拦截时长(秒), 0:永久拦截
_M.set_ccip = function(ip, timeout)
    return adapter:set_ccip(ip, timeout)
end

---检测指定IP是否已被CC拦截;
---     true:已拦截;  false:未拦截;
---@param ip string 待检测IP
---@return boolean
_M.ccip_exist = function(ip)
    return adapter:ccip_exist(ip)
end

---清除CC检测已拦截的IP
---@param ip nil | string  待清除的IP(可选值： nil / "all" / "具体IP") nil等同于"all"，所有已拦截IP都清除
_M.clean_ccip = function(ip)
    return adapter:clean_ccip(ip)
end

return _M
