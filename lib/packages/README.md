> 此目录存放第三方lua模块

```shell
# 文件结构
./easy_ngx_waf/packages/  # 第三方lua模块目录
    |   etlua.lua           # 一个简单的lua模板解析库(https://github.com/leafo/etlua)
    |   ipmatcher.lua       # 校验IP(v4 v6)是否属于指定网段(https://github.com/iresty/lua-resty-ipmatcher)
    |   ipmatcher_win.lua   # 同ipmatcher.lua一样,修改为windows下专用
    |   iputils.lua         # 校验IPv4是否属于指定网段(https://github.com/hamishforbes/lua-resty-iputils)
    |   maxminddb.lua       # 解析maxminddb数据的库(https://github.com/anjia0532/lua-resty-maxminddb)
    |   moses.lua           # LUA常用工具库(http://github.com/Yonaba/Moses)
    |   moses_min.lua       # moses.lua代码压缩版
    |   README.md
    |
    |+---clib_win/    # windows下的C库目录
    |       lfs.dll          # lfs在windows下的库文件
    |
    +---libmaxminddb_win/
    |       libmaxminddb.dll  # windows下的maxminddb库文件 geoip解析库(方便windows下开发调试用)
    |
    \---resty/
        |   template.lua     # lua-resty-template 模板解析库(https://github.com/bungle/lua-resty-template)
        |
        \---template/        # lua-resty-template 依赖文件
                ......

```
