## 自定义规则

### 目录说明

> 当前 `easy_ngx_waf/rules_custom/` 为使用者自定义规则目录，目录包含
>
> - `easy_ngx_waf/rules_custom/*.rule`  自定义常规规则文件(也可以没有任何常规规则文件)
>
> -  `easy_ngx_waf/rules_custom/simple/*` 自定义简单规则文件(各个文件名不可修改/删除，只可修改内容)
>
> -  `easy_ngx_waf/rules_custom/spider/*` 自定义蜘蛛数据文件(也可以没有任何文件)
>
> 目录结构及文件功能同 `easy_ngx_waf/rules/` 规则主目录相同
>
> 
>
> 为什么要多一级 `easy_ngx_waf/rules_custom/` 目录呢?
>
> 因为笔者在此是想维护一个 `easy_ngx_waf_rule` 的项目，该项目中包含规则主目录的 `easy_ngx_waf/rules/*.rule` 以及 `easy_ngx_waf/rules/simple/*`  `easy_ngx_waf/rules/spider/*` ，这些规则文件会不断完善更新，所以在更新 `easy_ngx_waf_rule` 时可方便覆盖规则主目录，而当前 `rules_custom/` 目录可不受影响。(在此也希望各位大佬能提供安全方面的waf规则，直接提交issues即可)
>


### 规则文件

> 常规规则文件可有多个，文件名可自定义，但文件名须以 `.rule` 结尾。
> 
> 简单规则文件放置在 `./simple/` 目录，文件名不可更改。
> 
> 常规规则文件及简单规则文件说明请见规则主目录 `easy_ngx_waf/rules/` 下 README 
