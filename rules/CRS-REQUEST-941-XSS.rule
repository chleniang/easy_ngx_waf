[

## -=[ XSS Filters - Category 1 ]=-
## http://xssplayground.net23.net/xssfilter.html
## script tag based XSS vectors, e.g., <script> alert(1)</script>
    {
        "name": "CRS-XSS-Category1-941110",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "REQUEST_FILENAME",
            "USER_AGENT",
            "REQUEST_HEADERS:Referer",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<script[^>]*>[\s\S]*?",
        "act": "deny",
        "log": "on",
        "msg": "script tag based XSS vectors. ID:941110"
    },

## -=[ XSS Filters - Category 2 ]=-
## XSS vectors making use of event handlers like onerror, onload etc, e.g., <body onload="alert(1)">
    {
        "name": "CRS-XSS-Category2-941120",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "REQUEST_FILENAME",
            "USER_AGENT",
            "REQUEST_HEADERS:Referer",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "[\s\"'`;\/0-9=\x0B\x09\x0C\x3B\x2C\x28\x3B]on[a-zA-Z]+[\s\x0B\x09\x0C\x3B\x2C\x28\x3B]*?=",
        "act": "deny",
        "log": "on",
        "msg": "XSS vectors making use of event handlers like onerror, onload etc. ID:941120"
    },

## -=[ XSS Filters - Category 3 ]=-
##
## Regexp generated from util/regexp-assemble/regexp-941130.data using Regexp::Assemble.
## To rebuild the regexp:
##   cd util/regexp-assemble
##   ./regexp-assemble.pl regexp-941130.data
    {
        "name": "CRS-XSS-Category3-941130",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "USER_AGENT",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "[\s\S](?:!ENTITY\s+(?:\S+|%\s+\S+)\s+(?:PUBLIC|SYSTEM)|x(?:link:href|html|mlns)|data:text\/html|pattern\b.*?=|formaction|\@import|;base64)\b",
        "act": "deny",
        "log": "on",
        "msg": "XSS Attribute Vector. ID:941130"
    },

## -=[ XSS Filters - Category 4 ]=-
## XSS vectors making use of javascript uri and tags, e.g., <p style="background:url(javascript:alert(1))">
    {
        "name": "CRS-XSS-Category4-941140",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "USER_AGENT",
            "REQUEST_HEADERS:Referer",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?:<(?:(?:apple|objec)t|isindex|embed|style|form|meta)\b[^>]*?>[\s\S]*?|(?:=|U\s*?R\s*?L\s*?\()\s*?[^>]*?\s*?S\s*?C\s*?R\s*?I\s*?P\s*?T\s*?:)",
        "act": "deny",
        "log": "on",
        "msg": "XSS vectors making use of javascript uri and tags. ID:941140"
    },

## -=[ NoScript XSS Filters ]=-
## Ref: http://noscript.net/
##
## [NoScript InjectionChecker] HTML injection
    {
        "name": "CRS-XSS-NoScript-HTML-941160",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "USER_AGENT",
            "REQUEST_HEADERS:Referer",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:(?:<\w[\s\S]*[\s\/]|['\"](?:[\s\S]*[\s\/])?)(?:on(?:d(?:e(?:vice(?:(?:orienta|mo)tion|proximity|found|light)|livery(?:success|error)|activate)|r(?:ag(?:e(?:n(?:ter|d)|xit)|(?:gestur|leav)e|start|drop|over)|op)|i(?:s(?:c(?:hargingtimechange|onnect(?:ing|ed))|abled)|aling)|ata(?:setc(?:omplete|hanged)|(?:availabl|chang)e|error)|urationchange|ownloading|blclick)|Moz(?:M(?:agnifyGesture(?:Update|Start)?|ouse(?:PixelScroll|Hittest))|S(?:wipeGesture(?:Update|Start|End)?|crolledAreaChanged)|(?:(?:Press)?TapGestur|BeforeResiz)e|EdgeUI(?:C(?:omplet|ancel)|Start)ed|RotateGesture(?:Update|Start)?|A(?:udioAvailable|fterPaint))|c(?:o(?:m(?:p(?:osition(?:update|start|end)|lete)|mand(?:update)?)|n(?:t(?:rolselect|extmenu)|nect(?:ing|ed))|py)|a(?:(?:llschang|ch)ed|nplay(?:through)?|rdstatechange)|h(?:(?:arging(?:time)?ch)?ange|ecking)|(?:fstate|ell)change|u(?:echange|t)|l(?:ick|ose))|s(?:t(?:a(?:t(?:uschanged|echange)|lled|rt)|k(?:sessione|comma)nd|op)|e(?:ek(?:complete|ing|ed)|(?:lec(?:tstar)?)?t|n(?:ding|t))|(?:peech|ound)(?:start|end)|u(?:ccess|spend|bmit)|croll|how)|m(?:o(?:z(?:(?:pointerlock|fullscreen)(?:change|error)|(?:orientation|time)change|network(?:down|up)load)|use(?:(?:lea|mo)ve|o(?:ver|ut)|enter|wheel|down|up)|ve(?:start|end)?)|essage|ark)|a(?:n(?:imation(?:iteration|start|end)|tennastatechange)|fter(?:(?:scriptexecu|upda)te|print)|udio(?:process|start|end)|d(?:apteradded|dtrack)|ctivate|lerting|bort)|b(?:e(?:fore(?:(?:(?:de)?activa|scriptexecu)te|u(?:nload|pdate)|p(?:aste|rint)|c(?:opy|ut)|editfocus)|gin(?:Event)?)|oun(?:dary|ce)|l(?:ocked|ur)|roadcast|usy)|DOM(?:Node(?:Inserted(?:IntoDocument)?|Removed(?:FromDocument)?)|(?:CharacterData|Subtree)Modified|A(?:ttrModified|ctivate)|Focus(?:Out|In)|MouseScroll)|r(?:e(?:s(?:u(?:m(?:ing|e)|lt)|ize|et)|adystatechange|pea(?:tEven)?t|movetrack|trieving|ceived)|ow(?:s(?:inserted|delete)|e(?:nter|xit))|atechange)|p(?:op(?:up(?:hid(?:den|ing)|show(?:ing|n))|state)|a(?:ge(?:hide|show)|(?:st|us)e|int)|ro(?:pertychange|gress)|lay(?:ing)?)|t(?:ouch(?:(?:lea|mo)ve|en(?:ter|d)|cancel|start)|ransition(?:cancel|end|run)|ime(?:update|out)|ext)|u(?:s(?:erproximity|sdreceived)|p(?:gradeneeded|dateready)|n(?:derflow|load))|f(?:o(?:rm(?:change|input)|cus(?:out|in)?)|i(?:lterchange|nish)|ailed)|l(?:o(?:ad(?:e(?:d(?:meta)?data|nd)|start)|secapture)|evelchange|y)|g(?:amepad(?:(?:dis)?connected|button(?:down|up)|axismove)|et)|e(?:n(?:d(?:Event|ed)?|abled|ter)|rror(?:update)?|mptied|xit)|i(?:cc(?:cardlockerror|infochange)|n(?:coming|valid|put))|o(?:(?:(?:ff|n)lin|bsolet)e|verflow(?:changed)?|pen)|SVG(?:(?:Unl|L)oad|Resize|Scroll|Abort|Error|Zoom)|h(?:e(?:adphoneschange|l[dp])|ashchange|olding)|v(?:o(?:lum|ic)e|ersion)change|w(?:a(?:it|rn)ing|heel)|key(?:press|down|up)|(?:AppComman|Loa)d|no(?:update|match)|Request|zoom)|s(?:tyle|rc)|background|formaction|lowsrc|ping)[\s\x08]*?=|<[^\w<>]*(?:[^<>\"'\s]*:)?[^\w<>]*\W*?(?:(?:a\W*?(?:n\W*?i\W*?m\W*?a\W*?t\W*?e|p\W*?p\W*?l\W*?e\W*?t|u\W*?d\W*?i\W*?o)|b\W*?(?:i\W*?n\W*?d\W*?i\W*?n\W*?g\W*?s|a\W*?s\W*?e|o\W*?d\W*?y)|i?\W*?f\W*?r\W*?a\W*?m\W*?e|o\W*?b\W*?j\W*?e\W*?c\W*?t|i\W*?m\W*?a?\W*?g\W*?e?|e\W*?m\W*?b\W*?e\W*?d|p\W*?a\W*?r\W*?a\W*?m|v\W*?i\W*?d\W*?e\W*?o|l\W*?i\W*?n\W*?k)[^>\w]|s\W*?(?:c\W*?r\W*?i\W*?p\W*?t|t\W*?y\W*?l\W*?e|e\W*?t[^>\w]|v\W*?g)|m\W*?(?:a\W*?r\W*?q\W*?u\W*?e\W*?e|e\W*?t\W*?a[^>\w])|f\W*?o\W*?r\W*?m))",
        "act": "next",
        "log": "off",
        "msg": "NoScript XSS InjectionChecker: HTML Injection. ID:941160"
    },

## [NoScript InjectionChecker] Attributes injection
    {
        "name": "CRS-XSS-NoScript-Attributes-941170",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "USER_AGENT",
            "REQUEST_HEADERS:Referer",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?:\W|^)(?:javascript:(?:[\s\S]+[=\\\(\[\.<]|[\s\S]*?(?:\bname\b|\\[ux]\d))|data:(?:(?:[a-z]\w+\/\w[\w+-]+\w)?[;,]|[\s\S]*?;[\s\S]*?\b(?:base64|charset=)|[\s\S]*?,[\s\S]*?<[\s\S]*?\w[\s\S]*?>))|@\W*?i\W*?m\W*?p\W*?o\W*?r\W*?t\W*?(?:\/\*[\s\S]*?)?(?:[\"']|\W*?u\W*?r\W*?l[\s\S]*?\()|\W*?-\W*?m\W*?o\W*?z\W*?-\W*?b\W*?i\W*?n\W*?d\W*?i\W*?n\W*?g[\s\S]*?:[\s\S]*?\W*?u\W*?r\W*?l[\s\S]*?\(",
        "act": "deny",
        "log": "on",
        "msg": "NoScript XSS InjectionChecker: Attribute Injection. ID:941170"
    },
    
## [Blacklist Keywords from Node-Validator]
## https://raw.github.com/chriso/node-validator/master/validator.js
    {
        "name": "CRS-XSS-NoScript-Attributes-941180",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(document\.cookie)|(document\.write)|(\.parentnode)|(\.innerhtml)|(window\.location)|(-moz-binding)|(<!-- -->)|(<!\[cdata\[)",
        "act": "deny",
        "log": "on",
        "msg": "NoScript XSS InjectionChecker: Attribute Injection. ID:941170"
    },
    
## -=[ XSS Filters from IE ]=-
## Ref: http://blogs.technet.com/srd/archive/2008/08/18/ie-8-CRS-XSS-filter-architecture-implementation.aspx
## Ref: http://xss.cx/examples/ie/internet-exploror-ie9-CRS-XSS-filter-rules-example-regexp-mshtmldll.txt
    {
        "name": "CRS-XSS-IE-941190",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:<style.*?>.*?(?:@[i\\\\]|(?:[:=]|&#x?0*(?:58|3A|61|3D);?).*?(?:[(\\\\]|&#x?0*(?:40|28|92|5C);?)))",
        "act": "deny",
        "log": "on",
        "msg": "XSS Filters from IE. ID:941190"
    },

## 
    {
        "name": "CRS-XSS-941200",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:<.*[:]?vmlframe.*?[\s/+]*?src[\s/+]*=)",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941200"
    },

## 
    {
        "name": "CRS-XSS-941210",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:(?:j|&#x?0*(?:74|4A|106|6A);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:a|&#x?0*(?:65|41|97|61);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:v|&#x?0*(?:86|56|118|76);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:a|&#x?0*(?:65|41|97|61);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:s|&#x?0*(?:83|53|115|73);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:c|&#x?0*(?:67|43|99|63);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:r|&#x?0*(?:82|52|114|72);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:i|&#x?0*(?:73|49|105|69);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:p|&#x?0*(?:80|50|112|70);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:t|&#x?0*(?:84|54|116|74);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?::|&(?:#x?0*(?:58|3A);?|colon;)).)",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941210"
    },

## 
    {
        "name": "CRS-XSS-941220",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:(?:v|&#x?0*(?:86|56|118|76);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:b|&#x?0*(?:66|42|98|62);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:s|&#x?0*(?:83|53|115|73);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:c|&#x?0*(?:67|43|99|63);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:r|&#x?0*(?:82|52|114|72);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:i|&#x?0*(?:73|49|105|69);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:p|&#x?0*(?:80|50|112|70);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?:t|&#x?0*(?:84|54|116|74);?)(?:\t|&(?:#x?0*(?:9|13|10|A|D);?|tab;|newline;))*(?::|&(?:#x?0*(?:58|3A);?|colon;)).)",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941220"
    },

## 
    {
        "name": "CRS-XSS-941230",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<EMBED[\s/+].*?(?:src|type).*?=",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941230"
    },

## 
    {
        "name": "CRS-XSS-941240",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<[?]?import[\s\/+\S]*?implementation[\s\/+]*?=",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941240"
    },

## 
    {
        "name": "CRS-XSS-941250",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:<META[\s/+].*?http-equiv[\s/+]*=[\s/+]*[\"'`]?(?:(?:c|&#x?0*(?:67|43|99|63);?)|(?:r|&#x?0*(?:82|52|114|72);?)|(?:s|&#x?0*(?:83|53|115|73);?)))",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941250"
    },

## 
    {
        "name": "CRS-XSS-941260",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:<META[\s/+].*?charset[\s/+]*=)",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941260"
    },

## 
    {
        "name": "CRS-XSS-941270",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i)<LINK[\s/+].*?href[\s/+]*=",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941270"
    },

## 
    {
        "name": "CRS-XSS-941280",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<BASE[\s/+].*?href[\s/+]*=",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941280"
    },

## 
    {
        "name": "CRS-XSS-941290",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<APPLET[\s/+>]",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941290"
    },

## 
    {
        "name": "CRS-XSS-941300",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<OBJECT[\s/+].*?(?:type|codetype|classid|code|data)[\s/+]*=",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941300"
    },

## https://www.owasp.org/www-community/CRS-XSS-filter-evasion-cheatsheet
## US-ASCII encoding bypass listed on XSS filter evasion
## Reported by Mazin Ahmed
    {
        "name": "CRS-XSS-941310",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "\xbc[^\xbe>]*[\xbe>]|<[^\xbe]*\xbe",
        "act": "next",
        "log": "off",
        "msg": "US-ASCII Malformed Encoding XSS Filter - Attack Detected. ID:941310"
    },

## 
    {
        "name": "CRS-XSS-941350",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "\+ADw-.*(?:\+AD4-|>)|<.*\+AD4-",
        "act": "deny",
        "log": "on",
        "msg": "UTF-7 Encoding IE XSS - Attack Detected. ID:941350"
    },

## Defend against JSFuck and Hieroglyphy obfuscation of Javascript code
##
## https://en.wikipedia.org/wiki/JSFuck
## https://github.com/alcuadrado/hieroglyphy
##
## These JS obfuscations mostly aim for client side XSS exploits, hence the
## integration of this rule into the XSS rule group. But serverside JS could
## also be attacked via these techniques.
##
## Detection pattern / Core elements of JSFuck and Hieroglyphy are the
## following two items:
## !![]
## !+[]
##
## ModSecurity always transforms "+" into " " with query strings and the
## URLENCODE body processor (but not for JSON). So we need to check for
## the following patterns:
## !![]
## !+[]
## ! []
    {
        "name": "CRS-XSS-941360",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "![!+ ]\[\]",
        "act": "deny",
        "log": "on",
        "msg": "JSFuck / Hieroglyphy obfuscation detected. ID:941360"
    },

## Prevent 941180 bypass by using JavaScript global variables
## Refer to: https://www.secjuice.com/ypass-CRS-XSS-filters-using-javascript-global-variables/
##
## Examples:
##    - /?search=/?a=";+alert(self["document"]["cookie"]);//
##    - /?search=/?a=";+document+/*foo*/+.+/*bar*/+cookie;//
    {
        "name": "CRS-XSS-941370",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?:self|document|this|top|window)\s*(?:/\*|[\[)]).+?(?:\]|\*/)",
        "act": "deny",
        "log": "on",
        "msg": "JavaScript global variable found. ID:941370"
    },

## -=[ XSS Filters - Category 5 ]=-
## HTML attributes - src, style and href
    {
        "name": "CRS-XSS-941150",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "USER_AGENT",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "\b(?:s(?:tyle|rc)|href)\b[\s\S]*?=",
        "act": "next",
        "log": "off",
        "msg": "XSS Filter - Category 5: Disallowed HTML Attributes. ID:941150"
    },

## Detect tags that are the most common direct HTML injection points.
## This rule is also triggered by the following exploit(s):
## [ SAP CRM Java vulnerability CVE-2018-2380 - Exploit tested: https://www.exploit-db.com/exploits/44292 ]
    {
        "name": "CRS-XSS-941320",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "<(?:a|abbr|acronym|address|applet|area|audioscope|b|base|basefront|bdo|bgsound|big|blackface|blink|blockquote|body|bq|br|button|caption|center|cite|code|col|colgroup|comment|dd|del|dfn|dir|div|dl|dt|em|embed|fieldset|fn|font|form|frame|frameset|h1|head|hr|html|i|iframe|ilayer|img|input|ins|isindex|kdb|keygen|label|layer|legend|li|limittext|link|listing|map|marquee|menu|meta|multicol|nobr|noembed|noframes|noscript|nosmartquotes|object|ol|optgroup|option|p|param|plaintext|pre|q|rt|ruby|s|samp|script|select|server|shadow|sidebar|small|spacer|span|strike|strong|style|sub|sup|table|tbody|td|textarea|tfoot|th|thead|title|tr|tt|u|ul|var|wbr|xml|xmp)\W",
        "act": "next",
        "log": "off",
        "msg": "Possible XSS Attack Detected - HTML Tag Handler. ID:941320"
    },

## 
    {
        "name": "CRS-XSS-941330",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?i:[\"'][ ]*(?:[^a-z0-9~_:' ]|in).*?(?:(?:l|\\\\u006C)(?:o|\\\\u006F)(?:c|\\\\u0063)(?:a|\\\\u0061)(?:t|\\\\u0074)(?:i|\\\\u0069)(?:o|\\\\u006F)(?:n|\\\\u006E)|(?:n|\\\\u006E)(?:a|\\\\u0061)(?:m|\\\\u006D)(?:e|\\\\u0065)|(?:o|\\\\u006F)(?:n|\\\\u006E)(?:e|\\\\u0065)(?:r|\\\\u0072)(?:r|\\\\u0072)(?:o|\\\\u006F)(?:r|\\\\u0072)|(?:v|\\\\u0076)(?:a|\\\\u0061)(?:l|\\\\u006C)(?:u|\\\\u0075)(?:e|\\\\u0065)(?:O|\\\\u004F)(?:f|\\\\u0066)).*?=)",
        "act": "deny",
        "log": "on",
        "msg": "IE XSS Filters - Attack Detected. ID:941330"
    },

## This rule is also triggered by the following exploit(s):
## [ SAP CRM Java vulnerability CVE-2018-2380 - Exploit tested: https://www.exploit-db.com/exploits/44292 ] 
    {
        "name": "CRS-XSS-941340",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "[\"\'][ ]*(?:[^a-z0-9~_:\' ]|in).+?[.].+?=",
        "act": "next",
        "log": "off",
        "msg": "IE XSS Filters - Attack Detected. ID:941340"
    },

## Defend against AngularJS client side template injection
##
## Of course, pure client-side AngularJS commands can not be intercepted.
## But once a command is sent to the server, the CRS will trigger.
##
## https://portswigger.net/blog/CRS-XSS-without-html-client-side-template-injection-with-angularjs
##
## Example payload:
## http://localhost/login?user=%20x%20%7B%7Bconstructor.constructor(%27alert(1)%27)()%7D%7D%20.%20ff
## Decoded argument:
## {{constructor.constructor('alert(1)')()}}
    {
        "name": "CRS-XSS-941380",
        "var": [
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "{{.*?}}",
        "act": "deny",
        "log": "on",
        "msg": "AngularJS client side template injection detected. ID:941380"
    }

]