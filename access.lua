local config = require("config")
local util = require("lib.util")
local host_lib = require("lib.rule_core.host")

if g_waf_enable then
    local host = ngx.var.host
    local is_ex_host = host_lib.is_ex_host(host, "all")
    if not is_ex_host then
        local ip = util.get_client_ip()
        local ua = util.get_user_agent()

        -- 完整的 含参数 /a/b/test.html?ip=192.168.25.5&ua=applebot
        local request_uri = ngx.var.request_uri

        -- 不含参数 /a/b/test.html
        local uri = ngx.var.uri

        local request_filename = ngx.var.request_filename

        gfn_ip_next_check(ip)
        if gfn_ip_white_check(ip, host) then
            -- 白名单命中(啥也不做,直接放行)
        elseif gfn_ip_black_check(ip, host) then
            -- 黑名单命中
            gfn_deny_response(config.ip_black_deny_return)
        elseif gfn_user_agent_white_check(ua, host) then
            -- USER-AGENT白名单命中(啥也不做,直接放行)
        elseif gfn_url_white_check(request_uri, request_filename) then
            -- URL白名单命中(啥也不做,直接放行)
        elseif gfn_url_check(request_uri, request_filename) then
            -- URL命中
            gfn_deny_response(config.url_deny_return)
        elseif gfn_seo_spider_check(ua, ip) then
            -- SEO蜘蛛命中(啥也不做,直接放行)
        elseif gfn_ip_foreign_check(ip, host) then
            -- 境外IP命中
            gfn_deny_response(config.ip_foreign_deny_return)
        elseif gfn_user_agent_check(ua, host) then
            -- USER-AGENT命中
            gfn_deny_response(config.user_agent_deny_return)
        elseif gfn_cc_check(ip, uri, host) then
            -- CC命中
            gfn_deny_response(config.cc_deny_return)
        elseif gfn_cookie_check(host) then
            -- COOKIE命中
            gfn_deny_response(config.cookie_deny_return)
        elseif gfn_args_check(host) then
            -- ARGS命中
            gfn_deny_response(config.args_deny_return)
        elseif gfn_post_check(host) then
            -- POST命中
            gfn_deny_response(config.post_deny_return)
        elseif gfn_header_check(host) then
            -- HEADER校验命中
            gfn_deny_response(config.header_deny_return)
        end
    end
end
