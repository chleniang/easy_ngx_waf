local config = {}

-- 总开关
config.waf_enable = "on"

-- 是否记录waf拦截日志(拦截日志总开关)
config.waf_log = "on"
-- waf拦截日志路径(绝对路径，确保nginx用户有写权限;文件未生成前可手动创建或者所属目录需要有写权限)
--     文件名可自定义,只要nginx用户有写权限即可.
--     配置示例: "{nginx_log目录绝对路径}/easy_ngx_waf.log"
--     如果使用日志处理服务，一定要注意生成日志文件时的权限,所有者一定要是nginx用户，否则新日志无法写入。
--        笔者在日志这块踩的坑请参见 README 注意事项 -- WAF日志不记录 章节
config.waf_log_file = "/web_server/web/logs/easy_ngx_waf.log"

-- waf基础目录(绝对路径,库中规则目录及某些文件路径依赖此路径)
--     示例: "/web_server/tools/easy_ngx_waf"
--           "D:\\server\\easy_ngx_waf"
config.waf_base_path = "/web_server/tools/easy_ngx_waf"

-- WAF排除域名列表(host_exclude)
-- 可在此设置不参与WAF检测的域名(可设置全局排除,也可针对各检测阶段排除)
--      排除域名设置注意事项
--          "abc.com" => 具体域名，只针对 "abc.com" 域名排除，"www.abc.com" 还是参与waf检测的
--          "*.abc.com" => 通配符域名，针对所有 "abc.com" 域名("abc.com"  "www.abc.com"  "x.y.abc.com")都排除
config.host_x = {

    -- "all" 全局排除域名列表
    --      包含在此列表的域名直接跳过所有WAF检测
    all = {
        "*.easy_ngx_waf.xxx",
        "localhost",
    },

    -- "ip_white" IP白名单检测阶段排除域名列表
    ip_white = {
        "www.example.xxx",
        "*.example2.xxx",
    },

    -- "ip_black" IP黑名单检测阶段排除域名列表
    ip_black = {
    },

    -- "ip_foreign" 境外IP检测阶段排除域名列表
    ip_foreign = {
    },

    -- "ua" user-agent检测阶段排除域名列表(同时也不参与ua白名单检测)
    ua = {
    },

    -- "cookie" cookie检测阶段排除域名列表
    cookie = {
    },

    -- "cc" cc检测阶段排除域名列表
    cc = {
    },

    -- "args" args检测阶段(也就是get参数检测)排除域名列表
    args = {
    },

    -- "post" post检测阶段排除域名列表
    post = {
    },

    -- "header" header检测阶段排除域名列表
    header = {
    },

} -- END config.host_x


-- libmaxminddb编译安装后的库文件路径(绝对路径)
-- IP归属地区检测
--   linux下需要先安装libmaxminddb(项目地址 https://github.com/maxmind/libmaxminddb)
--       默认安装路径 "/usr/local/lib/libmaxminddb.so"
--   windows下已预置了动态库 config.waf_base_path.."\\lib\\packages\\libmaxminddb_win\\libmaxminddb.dll"
config.libmaxminddb_path = "/usr/local/lib/libmaxminddb.so"


-- IP白名单: 所有规则中, "var":"ip"  "act":"allow" 的都算作IP白名单(不管是 ip_match, ip_region 运算)
--      "op":"ip_match"  时, 规则 value 可以是符合CIDR规则的IP
--      "op":"ip_region" 时, 规则 value 可以是所在地区的代码(如: "CN","MO"...)
-- 是否开启IP白名单校验(白名单命中,后续校验都不做)
config.ip_white_check = "on"
-- IP白名单命中时是否记录日志(推荐关闭,可减少日志);此设置只针对简单规则有效,常规规则中有单独的日志开关;
config.ip_white_log = "off"


-- IP黑名单: 所有规则中, "var":"ip"  "act":"deny" 的都算作IP黑名单(不管是 ip_match, ip_region 运算)
--      "op":"ip_match"  时, 规则 value 可以是符合CIDR规则的IP
--      "op":"ip_region" 时, 规则 value 可以是所在地区的代码(如: "US","TW"...)
-- 是否开启IP黑名单校验
config.ip_black_check = "on"
-- IP黑名单命中时是否记录日志(推荐关闭,可减少日志);此设置只针对简单规则有效,常规规则中有单独的日志开关;
config.ip_black_log = "on"
-- IP黑名单校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.ip_black_deny_return = ngx.HTTP_NOT_FOUND


-- 是否开启user-agent白名单校验(开启可放行UA白名单)
config.user_agent_white_check = "off"
-- user-agent白名单命中时是否记录日志(关闭可减少日志);此设置只针对简单规则有效,常规规则中有单独的日志开关;
config.user_agent_white_log = "off"


-- 是否开启user-agent校验
config.user_agent_check = "on"
-- user-agent校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.user_agent_deny_return = "user_agent_deny"


-- 是否开启url白名单校验
config.url_white_check = "on"
-- url白名单命中时是否记录日志(关闭可减少日志);此设置只针对简单规则有效,常规规则中有单独的日志开关;
config.url_white_log = "off"


-- 是否开启url校验
config.url_check = "on"
-- url校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.url_deny_return = "url_deny"


-- 是否开启SEO蜘蛛校验
config.seo_spider_check = "on"
-- SEO蜘蛛校验命中时是否记录日志(关闭可减少日志)
config.seo_spider_log = "off"


-- 是否开启 禁止境外IP访问
config.ip_foreign_check = "on"
-- 禁止境外IP命中时是否记录日志(推荐关闭,可减少日志)
config.ip_foreign_log = "on"
-- 非境外IP地区列表(在这个列表中的地区为非境外IP),注意大小写
config.domestic_iso_code = {
    "CN",      -- 中国CODE
    "HK",      -- 香港CODE
    "MO",      -- 澳门CODE
    "TW",      -- 台湾省CODE
    "PRIVATE",  -- 内网CODE
    -- 内网IP段
    --      10.0.0.0 - 10.255.255.255
    --      172.16.0.0 - 172.31.255.255
    --      192.168.0.0 - 192.168.255.255
}
-- 禁止境外IP访问时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.ip_foreign_deny_return = "ip_foreign_deny"


-- 是否开启cookie校验
config.cookie_check = "on"
-- cookie校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.cookie_deny_return = "cookie_deny"


-- 是否开启args参数校验
config.args_check = "on"
-- args参数校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.args_deny_return = "args_deny"


-- 是否开启post参数校验
config.post_check = "on"
-- post参数校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.post_deny_return = "post_deny"


-- 是否开启header校验(此检测阶段已将user-agent cookie剥离)
config.header_check = "on"
-- header校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.header_deny_return = "header_deny"


-- 是否开启CC校验
config.cc_check = "on"
-- CC频率 单位:请求次数N/M秒 默认100/60:60秒请求100次即触发CC攻击
config.cc_rate = "100/60"
-- CC校验不通过时,返回内容 (可填写 HTTP错误码 / "default" / 自定义内容)
config.cc_deny_return = ngx.HTTP_NOT_FOUND
-- CC检测命中,封锁IP时长(单位:秒,如果为0则永久封锁)
config.cc_ip_lock_time = 300
-- CC防护使用的数据存储类型( "shared_dict" / "redis" )
--   如果使用shared_dict,需要在 http 段配置 lua_shared_dict easy_waf_cc_dict ?m;
--   如果使用redis,需要正确配置下方redis相关配置项
config.cc_dict_type = "shared_dict"

-- redis主机
config.redis_host = "127.0.0.1"
-- redis端口
config.redis_port = 6379
-- redis数据库编号(默认0号库,通常可选0-15)
config.redis_db_index = 0
-- redis密码(没有留空字符串)
config.redis_password = ""

-- 防火墙拦截后,默认返回响应状态码 及 html内容(如果各个拦截响应配置项设置为"default"时)
config.default_return_code = ngx.HTTP_NOT_FOUND
config.default_return_html = [[MMP...]]

return config
